const app = require("./app");
const fetchNews = require("./utils/FetchNews");
const cron = require("node-cron");

fetchNews();
cron.schedule("* * */24 * * *", () => {
  console.log("Running hourly update!");
  fetchNews();
});

// Constants
const PORT = 8080;
const HOST = "0.0.0.0";

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
