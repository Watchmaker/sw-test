"use strict";
const express = require("express");
const cors = require("cors");
require("./db/mongoose");
const New = require("./models/new");

const app = express();
app.use(express.json());
app.use(cors());

app.get("/news", async (req, res) => {
  try {
    const tasks = await New.find({ deleted: false })
      .sort({ created_timestamp: -1 })
      .limit(20);
    if (!tasks) {
      return res.status(200).send([]);
    }
    res.status(200).send(tasks);
  } catch (e) {
    res.status(500).send(e);
  }
});

app.put("/news/:id", async (req, res) => {
  try {
    const task = await New.findOne({
      objectID: req.params.id,
    });
    if (!task) {
      return res.status(404).send();
    }
    task.deleted = true;
    await task.save();
    res.status(200).send(task);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = app;
