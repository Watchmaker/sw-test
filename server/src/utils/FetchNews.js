const axios = require("axios");
const New = require("../models/new");

const fetchNews = () => {
  return axios
    .get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
    .then((data) => {
      const mapped = data.data.hits
        .map(
          ({
            objectID,
            title,
            story_title,
            created_at_i,
            author,
            url,
            story_url,
          }) => {
            return {
              objectID,
              title: title ? title : story_title ? story_title : null,
              created_timestamp: created_at_i,
              author,
              url: story_url ? story_url : url ? url : null,
            };
          }
        )
        .filter((element) => element.objectID);
      mapped.forEach((element) => {
        New.findOneAndUpdate(
          { objectID: element.objectID },
          element,
          {
            upsert: true,
            setDefaultsOnInsert: true,
          },
          (err) => {
            if (err) {
              console.log(`Error inserting document ${element.objectID}`);
            }
          }
        );
      });
    })
    .catch((e) => {
      console.log(e);
    });
};

module.exports = fetchNews;
