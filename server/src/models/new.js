const mongoose = require("mongoose");

const New = mongoose.model("New", {
  objectID: {
    type: Number,
    required: true,
  },
  title: {
    type: String,
  },
  author: {
    type: String,
  },
  created_timestamp: {
    type: Number,
    required: true,
  },
  url: {
    type: String,
  },
  deleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = New;
