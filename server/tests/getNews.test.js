const app = require("../src/app");
const request = require("supertest");
const New = require("../src/models/new");
const dummyNews = require("./__mocks__/dummyData");

beforeAll(() => {
  dummyNews.forEach((element) => {
    New.findOneAndUpdate(
      { objectID: element.objectID },
      element,
      {
        upsert: true,
        setDefaultsOnInsert: true,
      },
      (err) => {
        if (err) {
          console.log(`Error inserting document ${element.objectID}`);
        }
      }
    );
  });
});

test("Get News", async (done) => {
  expect.assertions(1);
  request(app)
    .get("/news")
    .expect(200)
    .end((err, res) => {
      if (err) {
        return done(err);
      }
      expect(res.body[1]).toEqual({
        ...dummyNews[1],
        deleted: false,
        _id: expect.any(String),
        __v: expect.any(Number),
      });
      done();
    });
});

test("Delete New", async (done) => {
  expect.assertions(1);
  request(app)
    .put(`/news/${dummyNews[5].objectID}`)
    .expect(200)
    .end((err, res) => {
      if (err) {
        return done(err);
      }
      expect(res.body).toEqual({
        ...dummyNews[5],
        deleted: true,
        _id: expect.any(String),
        __v: expect.any(Number),
      });
      done();
    });
});
