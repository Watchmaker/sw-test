const news = {
  hits: [
    {
      created_at: "2020-06-22T17:56:07.000Z",
      title: null,
      url: null,
      author: "erichocean",
      points: null,
      story_text: null,
      comment_text:
        "TSMC \u003ci\u003ealso\u003c/i\u003e has older process nodes running, there\u0026#x27;s literally no advantage to Intel failing to execute on their product roadmap; it\u0026#x27;s all downside.",
      num_comments: null,
      story_id: 23602626,
      story_title: "TSMC officially begins 5 nm production",
      story_url:
        "https://www.notebookcheck.net/TSMC-officially-begins-5-nm-production-Snapdragon-875-SoC-Snapdragon-X60-5G-modem-A14-Bionic-and-a-5-nm-AMD-high-end-GPU-incoming.477119.0.html",
      parent_id: 23603210,
      created_at_i: 1592848567,
      _tags: ["comment", "author_erichocean", "story_23602626"],
      objectID: "23603401",
      _highlightResult: {
        author: {
          value: "erichocean",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "TSMC \u003ci\u003ealso\u003c/i\u003e has older process \u003cem\u003enodes\u003c/em\u003e running, there's literally no advantage to Intel failing to execute on their product roadmap; it's all downside.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "TSMC officially begins 5 nm production",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.notebookcheck.net/TSMC-officially-begins-5-nm-production-Snapdragon-875-SoC-Snapdragon-X60-5G-modem-A14-Bionic-and-a-5-nm-AMD-high-end-GPU-incoming.477119.0.html",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T16:53:00.000Z",
      title: null,
      url: null,
      author: "tmcls",
      points: null,
      story_text: null,
      comment_text:
        "[Member of the statechannels.org team here.]\u003cp\u003eNot directly. State channels are similar to lightning, but there\u0026#x27;s a key difference:\u003cp\u003e* in lightning each payment is routed through the network independently, requiring participation from the intermediary nodes\u003cp\u003e* in a state channel the intermediary nodes are used to establish a channel and then the payments are direct\u003cp\u003eThe upshot is that in state channels, once the connection is established, the payments are truly peer-to-peer, and don\u0026#x27;t need any interaction with a third party.\u003cp\u003eWe use this property in Web3Torrent, as the payments are sent directly peer-to-peer on top of the webtorrent messaging layer. This wouldn\u0026#x27;t be possible with lightning, though you could probably find another way to accomplish something similar.",
      num_comments: null,
      story_id: 23602008,
      story_title:
        "Show HN: Web3Torrent – Adding Ethereum Micropayments to WebTorrent",
      story_url: "https://blog.statechannels.org/introducing-web3torrent/",
      parent_id: 23602222,
      created_at_i: 1592844780,
      _tags: ["comment", "author_tmcls", "story_23602008"],
      objectID: "23602484",
      _highlightResult: {
        author: {
          value: "tmcls",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "[Member of the statechannels.org team here.]\u003cp\u003eNot directly. State channels are similar to lightning, but there's a key difference:\u003cp\u003e* in lightning each payment is routed through the network independently, requiring participation from the intermediary \u003cem\u003enodes\u003c/em\u003e\u003cp\u003e* in a state channel the intermediary \u003cem\u003enodes\u003c/em\u003e are used to establish a channel and then the payments are direct\u003cp\u003eThe upshot is that in state channels, once the connection is established, the payments are truly peer-to-peer, and don't need any interaction with a third party.\u003cp\u003eWe use this property in Web3Torrent, as the payments are sent directly peer-to-peer on top of the webtorrent messaging layer. This wouldn't be possible with lightning, though you could probably find another way to accomplish something similar.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value:
            "Show HN: Web3Torrent – Adding Ethereum Micropayments to WebTorrent",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://blog.statechannels.org/introducing-web3torrent/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T16:19:44.000Z",
      title: null,
      url: null,
      author: "floren",
      points: null,
      story_text: null,
      comment_text:
        "The thing to keep in mind about supercomputers is that they are designed for particular applications. Nuclear weapons simulation, biological analysis (can we run simulations and get a vaccine?), cryptanalysis. These applications are \u003ci\u003eusually\u003c/i\u003e written in MPI, which is what coordinates communication between nodes.\u003cp\u003eIf you want to play with it at home, connect those laptops to an ethernet network and install MPI on them both--you should be able to find tutorials with a little web searching. Then you could probably run Linpack if you felt like it, but if you wanted to learn a little more about how HPC applications actually work, you could write your own MPI application. I wrote an MPI raytracer in college; it\u0026#x27;s a relatively quick project and, again, you can probably find a tutorial for it online.\u003cp\u003eEdit: Your cluster is going to suck terribly in comparison to \u0026quot;real\u0026quot; supercomputers, but scientists frequently do build their own small-scale clusters for application development. The actual big machines like Sequoia are all batch-processing and must be scheduled in advance, so it\u0026#x27;s a lot easier (and cheaper, supercomputer time costs money) to test your application locally in real-time.",
      num_comments: null,
      story_id: 23601098,
      story_title: "Japan Captures TOP500 Crown with Arm-Powered Supercomputer",
      story_url:
        "https://www.top500.org/news/japan-captures-top500-crown-arm-powered-supercomputer/",
      parent_id: 23601838,
      created_at_i: 1592842784,
      _tags: ["comment", "author_floren", "story_23601098"],
      objectID: "23601958",
      _highlightResult: {
        author: {
          value: "floren",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "The thing to keep in mind about supercomputers is that they are designed for particular applications. Nuclear weapons simulation, biological analysis (can we run simulations and get a vaccine?), cryptanalysis. These applications are \u003ci\u003eusually\u003c/i\u003e written in MPI, which is what coordinates communication between \u003cem\u003enodes\u003c/em\u003e.\u003cp\u003eIf you want to play with it at home, connect those laptops to an ethernet network and install MPI on them both--you should be able to find tutorials with a little web searching. Then you could probably run Linpack if you felt like it, but if you wanted to learn a little more about how HPC applications actually work, you could write your own MPI application. I wrote an MPI raytracer in college; it's a relatively quick project and, again, you can probably find a tutorial for it online.\u003cp\u003eEdit: Your cluster is going to suck terribly in comparison to \u0026quot;real\u0026quot; supercomputers, but scientists frequently do build their own small-scale clusters for application development. The actual big machines like Sequoia are all batch-processing and must be scheduled in advance, so it's a lot easier (and cheaper, supercomputer time costs money) to test your application locally in real-time.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Japan Captures TOP500 Crown with Arm-Powered Supercomputer",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.top500.org/news/japan-captures-top500-crown-arm-powered-supercomputer/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T16:11:11.000Z",
      title: null,
      url: null,
      author: "sushshshsh",
      points: null,
      story_text: null,
      comment_text:
        "Thank you for this helpful info. For comparison\u0026#x27;s sake, say that you wanted to make babby\u0026#x27;s first super computer in your house with 2 laptops. That is to say, each laptop is a single core x86 system with its own motherboard and ram and ssd, and they are connected to each other in some way (ethernet? usb?)\u003cp\u003eWhat software would one use to distribute some workload between these two nodes, what would the latency and bandwidth be bottlenecked by (the network connection?) and what other key statistics would be important in measuring exactly how this cheap $400 (used) set up compares to price\u0026#x2F;watt\u0026#x2F;flop performance for top 500 computers?",
      num_comments: null,
      story_id: 23601098,
      story_title: "Japan Captures TOP500 Crown with Arm-Powered Supercomputer",
      story_url:
        "https://www.top500.org/news/japan-captures-top500-crown-arm-powered-supercomputer/",
      parent_id: 23601391,
      created_at_i: 1592842271,
      _tags: ["comment", "author_sushshshsh", "story_23601098"],
      objectID: "23601838",
      _highlightResult: {
        author: {
          value: "sushshshsh",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "Thank you for this helpful info. For comparison's sake, say that you wanted to make babby's first super computer in your house with 2 laptops. That is to say, each laptop is a single core x86 system with its own motherboard and ram and ssd, and they are connected to each other in some way (ethernet? usb?)\u003cp\u003eWhat software would one use to distribute some workload between these two \u003cem\u003enodes\u003c/em\u003e, what would the latency and bandwidth be bottlenecked by (the network connection?) and what other key statistics would be important in measuring exactly how this cheap $400 (used) set up compares to price/watt/flop performance for top 500 computers?",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Japan Captures TOP500 Crown with Arm-Powered Supercomputer",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.top500.org/news/japan-captures-top500-crown-arm-powered-supercomputer/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T15:52:46.000Z",
      title: null,
      url: null,
      author: "01acheru",
      points: null,
      story_text: null,
      comment_text:
        "As blopeur said in another reply you need to feed data to the supercomputer, and as parallelizable as your algorithm may be some data might need to be shared between nodes at some point, just to name a couple of examples.\u003cp\u003eIf you connect a lot of cloud instances to act as a giant distribute computing cluster they’ll receive\u0026#x2F;share\u0026#x2F;return data via network interfaces or yet worse the internet, this is really really slower than what super computers do.\u003cp\u003eFor many applications that solution would be more efficient than a supercomputer, but for applications that need a supercomputer it would be inefficient. It just depends on what you need to do, but in any case it would be a computing cluster not a supercomputer.\u003cp\u003e(my two cents, I’m not into that field)",
      num_comments: null,
      story_id: 23601098,
      story_title: "Japan Captures TOP500 Crown with Arm-Powered Supercomputer",
      story_url:
        "https://www.top500.org/news/japan-captures-top500-crown-arm-powered-supercomputer/",
      parent_id: 23601317,
      created_at_i: 1592841166,
      _tags: ["comment", "author_01acheru", "story_23601098"],
      objectID: "23601589",
      _highlightResult: {
        author: {
          value: "01acheru",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "As blopeur said in another reply you need to feed data to the supercomputer, and as parallelizable as your algorithm may be some data might need to be shared between \u003cem\u003enodes\u003c/em\u003e at some point, just to name a couple of examples.\u003cp\u003eIf you connect a lot of cloud instances to act as a giant distribute computing cluster they’ll receive/share/return data via network interfaces or yet worse the internet, this is really really slower than what super computers do.\u003cp\u003eFor many applications that solution would be more efficient than a supercomputer, but for applications that need a supercomputer it would be inefficient. It just depends on what you need to do, but in any case it would be a computing cluster not a supercomputer.\u003cp\u003e(my two cents, I’m not into that field)",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Japan Captures TOP500 Crown with Arm-Powered Supercomputer",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.top500.org/news/japan-captures-top500-crown-arm-powered-supercomputer/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T15:12:37.000Z",
      title: null,
      url: null,
      author: "nojvek",
      points: null,
      story_text: null,
      comment_text:
        "When Deno initially mentioned they run Typescript directly I was intrigued. It’s like running GCC every time you want to invoke the script.\u003cp\u003eThat being said I run ts-node all the time with transpile-only and that is decently fast. Even on production where the minimal cost is incurred once when loading the module.\u003cp\u003eAs for their problems mentioned on the document. That Header class has a .d.ts conflict on an interface. That could be solved with having namespaces in .d.ts or having I prefix for class interfaces. We do this all the time in our codebase.\u003cp\u003eI’m not entirely sure why they have two copies of things. The doc doesn’t give much detail.\u003cp\u003eIf you want things to be fast like nodejs, then the other option is to use \u0026#x2F;\u0026#x2F;@ts-check comments on JS files, or with compiler allowJS and checkJS settings. Webpack checks all their JS.\u003cp\u003eTSC now also has an option to automatically generate .d.ts from JS files with jsdoc comments. With declarationOnly compiler flag. Many existing JS projects already do that.\u003cp\u003eSo TLDR is. You don’t have to ditch Typescript totally. Typescript can purely just be a checker that you run at CI time and it will work quite well with existing JS code. Although if you want super strict safety the .ts syntax is less verbose and nicer to write. Even then transpile only mode is much faster than running full tsc.\u003cp\u003eRemember: TS is a superset of JS. All JS is valid typescript. Sometimes we gotta  structure the JS properly and that’s where the issue is rather than the typesystem.",
      num_comments: null,
      story_id: 23592483,
      story_title: "Deno will stop using TypeScript",
      story_url: "https://startfunction.com/deno-will-stop-using-typescript/",
      parent_id: 23592483,
      created_at_i: 1592838757,
      _tags: ["comment", "author_nojvek", "story_23592483"],
      objectID: "23601106",
      _highlightResult: {
        author: {
          value: "nojvek",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "When Deno initially mentioned they run Typescript directly I was intrigued. It’s like running GCC every time you want to invoke the script.\u003cp\u003eThat being said I run ts-node all the time with transpile-only and that is decently fast. Even on production where the minimal cost is incurred once when loading the module.\u003cp\u003eAs for their problems mentioned on the document. That Header class has a .d.ts conflict on an interface. That could be solved with having namespaces in .d.ts or having I prefix for class interfaces. We do this all the time in our codebase.\u003cp\u003eI’m not entirely sure why they have two copies of things. The doc doesn’t give much detail.\u003cp\u003eIf you want things to be fast like \u003cem\u003enodejs\u003c/em\u003e, then the other option is to use //@ts-check comments on JS files, or with compiler allowJS and checkJS settings. Webpack checks all their JS.\u003cp\u003eTSC now also has an option to automatically generate .d.ts from JS files with jsdoc comments. With declarationOnly compiler flag. Many existing JS projects already do that.\u003cp\u003eSo TLDR is. You don’t have to ditch Typescript totally. Typescript can purely just be a checker that you run at CI time and it will work quite well with existing JS code. Although if you want super strict safety the .ts syntax is less verbose and nicer to write. Even then transpile only mode is much faster than running full tsc.\u003cp\u003eRemember: TS is a superset of JS. All JS is valid typescript. Sometimes we gotta  structure the JS properly and that’s where the issue is rather than the typesystem.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Deno will stop using TypeScript",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://startfunction.com/deno-will-stop-using-typescript/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T15:07:24.000Z",
      title: null,
      url: null,
      author: "headmelted",
      points: null,
      story_text: null,
      comment_text:
        "Most of my work outside of the day job is developed on x86 and deployed on ARM.\u003cp\u003eUnless you\u0026#x27;re talking about native code (and even then, I\u0026#x27;ve written in the past about ways this can be managed more easily), then \u003ci\u003eno\u003c/i\u003e, it really doesn\u0026#x27;t matter.\u003cp\u003eIf you\u0026#x27;re developing in NodeJS, Ruby, .NET, Python, Java or virtually any other interpreted or JIT-ed language, you were \u003ci\u003enever\u003c/i\u003e building for an architecture, you were building for a runtime, and the architecture is as irrelevant to you as it ever was.",
      num_comments: null,
      story_id: 23597403,
      story_title: "ARM Mac Impact on Intel",
      story_url: "https://mondaynote.com/arm-mac-impact-on-intel-9641a8e73dca",
      parent_id: 23598325,
      created_at_i: 1592838444,
      _tags: ["comment", "author_headmelted", "story_23597403"],
      objectID: "23601045",
      _highlightResult: {
        author: {
          value: "headmelted",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "Most of my work outside of the day job is developed on x86 and deployed on ARM.\u003cp\u003eUnless you're talking about native code (and even then, I've written in the past about ways this can be managed more easily), then \u003ci\u003eno\u003c/i\u003e, it really doesn't matter.\u003cp\u003eIf you're developing in \u003cem\u003eNodeJS\u003c/em\u003e, Ruby, .NET, Python, Java or virtually any other interpreted or JIT-ed language, you were \u003ci\u003enever\u003c/i\u003e building for an architecture, you were building for a runtime, and the architecture is as irrelevant to you as it ever was.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "ARM Mac Impact on Intel",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://mondaynote.com/arm-mac-impact-on-intel-9641a8e73dca",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T14:15:46.000Z",
      title: null,
      url: null,
      author: "hmwhy",
      points: null,
      story_text: null,
      comment_text:
        "Hello! Many apologies for having taken a while to reply, I finally found some time to sit down and do some tests because \u0026gt; 200 ms vs. \u0026lt; 10 ms is too big of a difference, and also because of what london_explore said:\u003cp\u003e\u0026gt; and if that performs badly, open a bug with V8 or other JavaScript engines to optimize it.\u003cp\u003eAnyhow! Just to clarify before moving on, I also did the tests in the Web Console. All numbers noted below are for the same (first) test I initially mentioned.\u003cp\u003eWhat I have done in exactly the same order as follows:\u003cp\u003e1. I first tested with a clean profile as you suggested and the times haven\u0026#x27;t changed. ~240 ms was observed.\u003cp\u003e2. I downloaded Firefox and tested with a clean installation of 77.0.1. ~240 ms was observed.\u003cp\u003e3. I removed both Firefox (77.0.1) and Firefox Developer edition (78.09b), and manually removed the ~\u0026#x2F;Library\u0026#x2F;Application Support\u0026#x2F;Firefox folder. I then reinstalled Firefox Developer edition (78.09b) and, without restoring my profiles, I tested again. ~240 ms was observed.\u003cp\u003e4. I re-ran exactly the same tests on NodeJS and Chrome. ~6 ms was observed.\u003cp\u003eAssuming that it\u0026#x27;s just me, I can\u0026#x27;t immediately think of what is causing this.\u003cp\u003eOtherwise, if it\u0026#x27;s not just my setup, could it be related to the operating system? I\u0026#x27;m currently running macOS 10.15.5. I don\u0026#x27;t have a machine with a different OS that I can test on at the moment, I should be able to try this in the next 2 hours and will update this post.\u003cp\u003eEdit: I have tested on Windows 10 as well and the issue is the same!",
      num_comments: null,
      story_id: 23587321,
      story_title: "Memory in JavaScript – Beyond Leaks (2019)",
      story_url:
        "https://medium.com/walkme-engineering/memory-in-javascript-beyond-leaks-8c1d697c655c",
      parent_id: 23599175,
      created_at_i: 1592835346,
      _tags: ["comment", "author_hmwhy", "story_23587321"],
      objectID: "23600403",
      _highlightResult: {
        author: {
          value: "hmwhy",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "Hello! Many apologies for having taken a while to reply, I finally found some time to sit down and do some tests because \u0026gt; 200 ms vs. \u0026lt; 10 ms is too big of a difference, and also because of what london_explore said:\u003cp\u003e\u0026gt; and if that performs badly, open a bug with V8 or other JavaScript engines to optimize it.\u003cp\u003eAnyhow! Just to clarify before moving on, I also did the tests in the Web Console. All numbers noted below are for the same (first) test I initially mentioned.\u003cp\u003eWhat I have done in exactly the same order as follows:\u003cp\u003e1. I first tested with a clean profile as you suggested and the times haven't changed. ~240 ms was observed.\u003cp\u003e2. I downloaded Firefox and tested with a clean installation of 77.0.1. ~240 ms was observed.\u003cp\u003e3. I removed both Firefox (77.0.1) and Firefox Developer edition (78.09b), and manually removed the ~/Library/Application Support/Firefox folder. I then reinstalled Firefox Developer edition (78.09b) and, without restoring my profiles, I tested again. ~240 ms was observed.\u003cp\u003e4. I re-ran exactly the same tests on \u003cem\u003eNodeJS\u003c/em\u003e and Chrome. ~6 ms was observed.\u003cp\u003eAssuming that it's just me, I can't immediately think of what is causing this.\u003cp\u003eOtherwise, if it's not just my setup, could it be related to the operating system? I'm currently running macOS 10.15.5. I don't have a machine with a different OS that I can test on at the moment, I should be able to try this in the next 2 hours and will update this post.\u003cp\u003eEdit: I have tested on Windows 10 as well and the issue is the same!",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Memory in JavaScript – Beyond Leaks (2019)",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://medium.com/walkme-engineering/memory-in-javascript-beyond-leaks-8c1d697c655c",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T13:39:20.000Z",
      title: null,
      url: null,
      author: "battery423",
      points: null,
      story_text: null,
      comment_text:
        "Coursera is not the central learning platform, its a course platform.\u003cp\u003eYou don\u0026#x27;t log in to see your learning graph which will give you the CS Bachelors Degree. You need to choose a course which will give you a CS Bachelors Degree.\u003cp\u003eImagine you log into Coursera and you want to have a comparable Degree. You would choose courses to learn \u0026#x27;nodes\u0026#x27; of your learning graph. But you could choose which one. You could learn the security topic from a course from someone from harvard and you could learn security topic from someone in germany.\u003cp\u003eYou choose your language, you choose your medium. Might be that a lot of text + 1-2 explain videos help you more then a 2h lecture.\u003cp\u003eMight be that you can choose between implementing a program or do a presentation.\u003cp\u003eIt might even be required for you to listen to a presentation from other students. Or create your own small programming tasks for others.\u003cp\u003eThis could become the best curated\u0026#x2F;crowd created biggest collection of educational material in the world.",
      num_comments: null,
      story_id: 23599449,
      story_title: "The University Is Like a CD in the Streaming Age",
      story_url:
        "https://www.theatlantic.com/ideas/archive/2020/06/university-like-cd-streaming-age/613291/",
      parent_id: 23599802,
      created_at_i: 1592833160,
      _tags: ["comment", "author_battery423", "story_23599449"],
      objectID: "23600041",
      _highlightResult: {
        author: {
          value: "battery423",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "Coursera is not the central learning platform, its a course platform.\u003cp\u003eYou don't log in to see your learning graph which will give you the CS Bachelors Degree. You need to choose a course which will give you a CS Bachelors Degree.\u003cp\u003eImagine you log into Coursera and you want to have a comparable Degree. You would choose courses to learn '\u003cem\u003enodes\u003c/em\u003e' of your learning graph. But you could choose which one. You could learn the security topic from a course from someone from harvard and you could learn security topic from someone in germany.\u003cp\u003eYou choose your language, you choose your medium. Might be that a lot of text + 1-2 explain videos help you more then a 2h lecture.\u003cp\u003eMight be that you can choose between implementing a program or do a presentation.\u003cp\u003eIt might even be required for you to listen to a presentation from other students. Or create your own small programming tasks for others.\u003cp\u003eThis could become the best curated/crowd created biggest collection of educational material in the world.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "The University Is Like a CD in the Streaming Age",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.theatlantic.com/ideas/archive/2020/06/university-like-cd-streaming-age/613291/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T12:55:33.000Z",
      title: null,
      url: null,
      author: "nbpname",
      points: null,
      story_text: null,
      comment_text:
        'I have similar results when running the code in the Firefox devtools console, however, running these benchmarks in a dedicated webpage does not show the results.\u003cp\u003eWhen using a dedicated web page, I see results which are in the same ranges as NodeJS and Chrome: (not comparable, as running on different hardware)\u003cp\u003e\u003cpre\u003e\u003ccode\u003e   Started data Local (original) foo.html:120:11\n  Finished data locality test run in  8.6800 ± 1.3095  ms. foo.html:144:11\n   Started data Non-local (original) foo.html:120:11\n  Finished data locality test run in  38.9400 ± 1.6195  ms. foo.html:144:11\n   Started data Local foo.html:120:11\n  Finished data locality test run in  6.6600 ± 1.1390  ms. foo.html:144:11\n   Started data Non-local foo.html:120:11\n  Finished data locality test run in  38.6400 ± 1.5861  ms. foo.html:144:11\n   Started data Non-local (sorted) foo.html:120:11\n  Finished data locality test run in  12.1800 ± 1.8334  ms. foo.html:144:11\n   Started data Local (object) foo.html:120:11\n  Finished data locality test run in  14.4500 ± 1.1492  ms. foo.html:144:11\n   Started data Non-local (object) foo.html:120:11\n  Finished data locality test run in  49.0000 ± 1.6576  ms. foo.html:144:11\n   Started data Non-local (sorted array of indices) foo.html:120:11\n  Finished data locality test run in  53.8500 ± 2.9725  ms.\n\u003c/code\u003e\u003c/pre\u003e\nI opened a bug against the devtools console of Firefox:\n\u003ca href="https:\u0026#x2F;\u0026#x2F;bugzilla.mozilla.org\u0026#x2F;show_bug.cgi?id=1647276" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;bugzilla.mozilla.org\u0026#x2F;show_bug.cgi?id=1647276\u003c/a\u003e',
      num_comments: null,
      story_id: 23587321,
      story_title: "Memory in JavaScript – Beyond Leaks (2019)",
      story_url:
        "https://medium.com/walkme-engineering/memory-in-javascript-beyond-leaks-8c1d697c655c",
      parent_id: 23598740,
      created_at_i: 1592830533,
      _tags: ["comment", "author_nbpname", "story_23587321"],
      objectID: "23599615",
      _highlightResult: {
        author: {
          value: "nbpname",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            'I have similar results when running the code in the Firefox devtools console, however, running these benchmarks in a dedicated webpage does not show the results.\u003cp\u003eWhen using a dedicated web page, I see results which are in the same ranges as \u003cem\u003eNodeJS\u003c/em\u003e and Chrome: (not comparable, as running on different hardware)\u003cp\u003e\u003cpre\u003e\u003ccode\u003e   Started data Local (original) foo.html:120:11\n  Finished data locality test run in  8.6800 ± 1.3095  ms. foo.html:144:11\n   Started data Non-local (original) foo.html:120:11\n  Finished data locality test run in  38.9400 ± 1.6195  ms. foo.html:144:11\n   Started data Local foo.html:120:11\n  Finished data locality test run in  6.6600 ± 1.1390  ms. foo.html:144:11\n   Started data Non-local foo.html:120:11\n  Finished data locality test run in  38.6400 ± 1.5861  ms. foo.html:144:11\n   Started data Non-local (sorted) foo.html:120:11\n  Finished data locality test run in  12.1800 ± 1.8334  ms. foo.html:144:11\n   Started data Local (object) foo.html:120:11\n  Finished data locality test run in  14.4500 ± 1.1492  ms. foo.html:144:11\n   Started data Non-local (object) foo.html:120:11\n  Finished data locality test run in  49.0000 ± 1.6576  ms. foo.html:144:11\n   Started data Non-local (sorted array of indices) foo.html:120:11\n  Finished data locality test run in  53.8500 ± 2.9725  ms.\n\u003c/code\u003e\u003c/pre\u003e\nI opened a bug against the devtools console of Firefox:\n\u003ca href="https://bugzilla.mozilla.org/show_bug.cgi?id=1647276" rel="nofollow"\u003ehttps://bugzilla.mozilla.org/show_bug.cgi?id=1647276\u003c/a\u003e',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Memory in JavaScript – Beyond Leaks (2019)",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://medium.com/walkme-engineering/memory-in-javascript-beyond-leaks-8c1d697c655c",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T11:49:53.000Z",
      title: null,
      url: null,
      author: "jaydadarkar",
      points: null,
      story_text: null,
      comment_text:
        'Thanks for sharing. But out of all frameworks out there for NodeJS, none could meet my requirement. None are easy to adapt. Hence I did make a small framework \u003ca href="https:\u0026#x2F;\u0026#x2F;www.npmjs.com\u0026#x2F;package\u0026#x2F;@jaydadarkar\u0026#x2F;nitromvc" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.npmjs.com\u0026#x2F;package\u0026#x2F;@jaydadarkar\u0026#x2F;nitromvc\u003c/a\u003e inspired by Laravel.',
      num_comments: null,
      story_id: 23508370,
      story_title: "Don't write your own framework",
      story_url: "https://stitcher.io/blog/dont-write-your-own-framework",
      parent_id: 23508370,
      created_at_i: 1592826593,
      _tags: ["comment", "author_jaydadarkar", "story_23508370"],
      objectID: "23599189",
      _highlightResult: {
        author: {
          value: "jaydadarkar",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            'Thanks for sharing. But out of all frameworks out there for \u003cem\u003eNodeJS\u003c/em\u003e, none could meet my requirement. None are easy to adapt. Hence I did make a small framework \u003ca href="https://www.npmjs.com/package/@jaydadarkar/nitromvc" rel="nofollow"\u003ehttps://www.npmjs.com/package/@jaydadarkar/nitromvc\u003c/a\u003e inspired by Laravel.',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Don't write your own framework",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://stitcher.io/blog/dont-write-your-own-framework",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T10:17:55.000Z",
      title: null,
      url: null,
      author: "hmwhy",
      points: null,
      story_text: null,
      comment_text:
        'I was a bit surprised by this. It could be because of a lack of understanding of JavaScript, but the biggest reason that surprised is because I was under the impression that the time it takes to access any element in a JavaScript Array is constant.\u003cp\u003eI took the code in that post, played around a little bit (code: \u003ca href="https:\u0026#x2F;\u0026#x2F;jsfiddle.net\u0026#x2F;x7sz1fhr" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;jsfiddle.net\u0026#x2F;x7sz1fhr\u003c/a\u003e), and the results of a hundred runs performed differently are shown below (NodeJS 12.6.1; Chrome 81.0.4044.122; Firefox Developer Edition 78.09b):\u003cp\u003e1. \u0026quot;Local\u0026quot;, original — NodeJS\u003ci\u003e\u003c/i\u003e\u003ci\u003e: 7.05 ± 13.54 ms; Chrome: 6.13 ± 13.91 ms; Firefox: 241 ± 6.\u003cp\u003e2. \u0026quot;Non-local\u0026quot;, original — NodeJS: 52.2 ± 1.4 ms; Chrome: 33.8 ± 1.1 ms; Firefox: 294 ± 2.\u003cp\u003e3. \u0026quot;Non-local (sorted)\u0026quot;, original — NodeJS: 10.3 ± 0.9 ms; Chrome: 8.58 ± 0.94 ms; Firefox: 200 ± 1.\u003cp\u003e4. \u0026quot;Local\u0026quot;, array — NodeJS: 5.05 ± 0.90 ms; Chrome: 4.22 ± 0.87 ms; Firefox: 193 ± 1.\u003cp\u003e5. \u0026quot;Non-local\u0026quot;, array — NodeJS: 51.9 ± 1.4 ms; Chrome: 34.7 ± 01.6 ms; Firefox: 252 ± 3.\u003cp\u003e6. \u0026quot;Local\u0026quot;, object — NodeJS: 6.50 ± 9.84 ms; Chrome: 5.80 ± 10.31 ms; Firefox: 201 ± 2.\u003cp\u003e7. \u0026quot;Non-local\u0026quot;, object — NodeJS: 58.1 ± 1.2 ms; Chrome: 42.1 ± 3.3 ms; Firefox: 247 ± 2.\u003cp\u003e8. \u0026quot;Non-local\u0026quot;, using sorted indices stored in an array — NodeJS: 57.6 ± 7.8 ms; Chrome: 35.9 ± 6.7 ms; Firefox: 155 ± 2.\u003cp\u003e\u003c/i\u003e\u003ci\u003e\u003c/i\u003e These numbers are inflated as an artefact of them being the first thing to run. Please see the comment of @minitech below.\u003cp\u003eThe first thing to note here is that the results obtained for Firefox are not a mistake: I have repeated by copy-and-pasting the same code a few times and, unfortunately, in all cases Firefox performed approximately 4–20 times slower than both Chrome and NodeJS. That\u0026#x27;s a bit disappointing because a couple of years ago I switched back to Firefox for its performance (I did some tests back then for my use cases before I decided). It is odd that the time difference is between 4–20 times, so it is possible that I did some non-standard things by quickly modifying the author\u0026#x27;s code.\u003cp\u003eA̶n̶o̶t̶h̶e̶r̶ ̶i̶n̶t̶e̶r̶e̶s̶t̶i̶n̶g̶ ̶p̶o̶i̶n̶t̶ ̶t̶o̶ ̶n̶o̶t̶e̶ ̶h̶e̶r̶e̶ ̶i̶s̶ ̶t̶h̶a̶t̶ ̶i̶n̶ ̶t̶h̶e̶ ̶s̶e̶c̶o̶n̶d̶ ̶h̶a̶l̶f̶ ̶o̶f̶ ̶t̶h̶e̶ ̶a̶r̶t̶i̶c̶l̶e̶,̶ ̶t̶h̶e̶ ̶a̶u̶t̶h̶o̶r̶ ̶m̶o̶d̶i̶f̶i̶e̶d̶ ̶t̶h̶e̶ ̶`̶l̶o̶c̶a̶l̶A̶c̶c̶e̶s̶s̶`̶ ̶f̶u̶n̶c̶t̶i̶o̶n̶ ̶t̶o̶ ̶a̶c̶c̶e̶p̶t̶ ̶a̶n̶ ̶o̶p̶t̶i̶o̶n̶a̶l̶ ̶a̶r̶r̶a̶y̶.̶ ̶T̶h̶a̶t̶ ̶m̶a̶y̶ ̶a̶c̶t̶u̶a̶l̶l̶y̶ ̶h̶a̶v̶e̶ ̶s̶i̶d̶e̶ ̶e̶f̶f̶e̶c̶t̶s̶ ̶t̶h̶a̶t̶ ̶a̶r̶e̶ ̶u̶n̶a̶c̶c̶o̶u̶n̶t̶e̶d̶ ̶f̶o̶r̶,̶ ̶w̶h̶i̶c̶h̶ ̶i̶s̶ ̶c̶l̶e̶a̶r̶ ̶u̶p̶o̶n̶ ̶c̶o̶m̶p̶a̶r̶i̶s̶o̶n̶ ̶o̶f̶ ̶E̶n̶t̶r̶y̶ ̶1̶ ̶a̶n̶d̶ ̶E̶n̶t̶r̶y̶ ̶3̶ ̶f̶o̶r̶ ̶a̶l̶l̶ ̶e̶n̶v̶i̶r̶o̶n̶m̶e̶n̶t̶s̶;̶ ̶a̶n̶d̶ ̶E̶n̶t̶r̶y̶ ̶2̶ ̶a̶n̶d̶ ̶E̶n̶t̶r̶y̶ ̶4̶ ̶f̶o̶r̶ ̶o̶n̶l̶y̶ ̶F̶i̶r̶e̶f̶o̶x̶.̶ ̶I̶f̶ ̶w̶e̶ ̶i̶g̶n̶o̶r̶e̶ ̶t̶h̶e̶ ̶o̶r̶d̶e̶r̶-̶o̶f̶-̶m̶a̶g̶n̶i̶t̶u̶d̶e̶ ̶d̶i̶f̶f̶e̶r̶e̶n̶c̶e̶ ̶a̶n̶d̶ ̶f̶o̶c̶u̶s̶ ̶o̶n̶ ̶t̶h̶e̶ ̶t̶r̶e̶n̶d̶s̶,̶ ̶i̶t̶ ̶a̶p̶p̶e̶a̶r̶s̶ ̶t̶h̶a̶t̶ ̶t̶h̶e̶ ̶o̶p̶t̶i̶m̶i̶s̶a̶t̶i̶o̶n̶ ̶u̶s̶e̶d̶ ̶i̶n̶ ̶d̶i̶f̶f̶e̶r̶e̶n̶t̶ ̶e̶n̶g̶i̶n̶e̶s̶ ̶s̶e̶e̶n̶ ̶h̶e̶r̶e̶ ̶c̶o̶r̶r̶o̶b̶o̶r̶a̶t̶e̶s̶ ̶w̶h̶a̶t̶ ̶@̶l̶o̶n̶d̶o̶n̶s̶_̶e̶x̶p̶l̶o̶r̶e̶ ̶h̶a̶s̶ ̶a̶l̶r̶e̶a̶d̶y̶ ̶p̶o̶i̶n̶t̶e̶d̶ ̶o̶u̶t̶.̶\u003cp\u003eThere is also a difference between the times for array and objects across different environment, with an object generally taking longer. This result isn\u0026#x27;t directly related to our primary interest here, but I thought it was interesting and worth pointing out because of object-related concept in JavaScript.\u003cp\u003eFinally, if we use a pre-sorted array of indices to access elements in the original array containing instances of `Boom`, we see a substantial reduction in the time for Firefox but not NodeJS and Chrome. This result was a bit \u0026quot;unexpected\u0026quot; but, again, this comes back to the point @londons_explore made.\u003cp\u003eEdit: updated code to allow the environment to be changed.\nEdit 2: Edited according to the of @minitech\u0026#x27;s comment.',
      num_comments: null,
      story_id: 23587321,
      story_title: "Memory in JavaScript – Beyond Leaks (2019)",
      story_url:
        "https://medium.com/walkme-engineering/memory-in-javascript-beyond-leaks-8c1d697c655c",
      parent_id: 23587321,
      created_at_i: 1592821075,
      _tags: ["comment", "author_hmwhy", "story_23587321"],
      objectID: "23598740",
      _highlightResult: {
        author: {
          value: "hmwhy",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "I was a bit surprised by this. It could be because of a lack of understanding of JavaScript, but the biggest reason that surprised is because I was under the impression that the time it takes to access any element in a JavaScript Array is constant.\u003cp\u003eI took the code in that post, played around a little bit (code: \u003ca href=\"https://jsfiddle.net/x7sz1fhr\" rel=\"nofollow\"\u003ehttps://jsfiddle.net/x7sz1fhr\u003c/a\u003e), and the results of a hundred runs performed differently are shown below (\u003cem\u003eNodeJS\u003c/em\u003e 12.6.1; Chrome 81.0.4044.122; Firefox Developer Edition 78.09b):\u003cp\u003e1. \u0026quot;Local\u0026quot;, original — \u003cem\u003eNodeJS\u003c/em\u003e\u003ci\u003e\u003c/i\u003e\u003ci\u003e: 7.05 ± 13.54 ms; Chrome: 6.13 ± 13.91 ms; Firefox: 241 ± 6.\u003cp\u003e2. \u0026quot;Non-local\u0026quot;, original — \u003cem\u003eNodeJS\u003c/em\u003e: 52.2 ± 1.4 ms; Chrome: 33.8 ± 1.1 ms; Firefox: 294 ± 2.\u003cp\u003e3. \u0026quot;Non-local (sorted)\u0026quot;, original — \u003cem\u003eNodeJS\u003c/em\u003e: 10.3 ± 0.9 ms; Chrome: 8.58 ± 0.94 ms; Firefox: 200 ± 1.\u003cp\u003e4. \u0026quot;Local\u0026quot;, array — \u003cem\u003eNodeJS\u003c/em\u003e: 5.05 ± 0.90 ms; Chrome: 4.22 ± 0.87 ms; Firefox: 193 ± 1.\u003cp\u003e5. \u0026quot;Non-local\u0026quot;, array — \u003cem\u003eNodeJS\u003c/em\u003e: 51.9 ± 1.4 ms; Chrome: 34.7 ± 01.6 ms; Firefox: 252 ± 3.\u003cp\u003e6. \u0026quot;Local\u0026quot;, object — \u003cem\u003eNodeJS\u003c/em\u003e: 6.50 ± 9.84 ms; Chrome: 5.80 ± 10.31 ms; Firefox: 201 ± 2.\u003cp\u003e7. \u0026quot;Non-local\u0026quot;, object — \u003cem\u003eNodeJS\u003c/em\u003e: 58.1 ± 1.2 ms; Chrome: 42.1 ± 3.3 ms; Firefox: 247 ± 2.\u003cp\u003e8. \u0026quot;Non-local\u0026quot;, using sorted indices stored in an array — \u003cem\u003eNodeJS\u003c/em\u003e: 57.6 ± 7.8 ms; Chrome: 35.9 ± 6.7 ms; Firefox: 155 ± 2.\u003cp\u003e\u003c/i\u003e\u003ci\u003e\u003c/i\u003e These numbers are inflated as an artefact of them being the first thing to run. Please see the comment of @minitech below.\u003cp\u003eThe first thing to note here is that the results obtained for Firefox are not a mistake: I have repeated by copy-and-pasting the same code a few times and, unfortunately, in all cases Firefox performed approximately 4–20 times slower than both Chrome and \u003cem\u003eNodeJS\u003c/em\u003e. That's a bit disappointing because a couple of years ago I switched back to Firefox for its performance (I did some tests back then for my use cases before I decided). It is odd that the time difference is between 4–20 times, so it is possible that I did some non-standard things by quickly modifying the author's code.\u003cp\u003eA̶n̶o̶t̶h̶e̶r̶ ̶i̶n̶t̶e̶r̶e̶s̶t̶i̶n̶g̶ ̶p̶o̶i̶n̶t̶ ̶t̶o̶ ̶n̶o̶t̶e̶ ̶h̶e̶r̶e̶ ̶i̶s̶ ̶t̶h̶a̶t̶ ̶i̶n̶ ̶t̶h̶e̶ ̶s̶e̶c̶o̶n̶d̶ ̶h̶a̶l̶f̶ ̶o̶f̶ ̶t̶h̶e̶ ̶a̶r̶t̶i̶c̶l̶e̶,̶ ̶t̶h̶e̶ ̶a̶u̶t̶h̶o̶r̶ ̶m̶o̶d̶i̶f̶i̶e̶d̶ ̶t̶h̶e̶ ̶`̶l̶o̶c̶a̶l̶A̶c̶c̶e̶s̶s̶`̶ ̶f̶u̶n̶c̶t̶i̶o̶n̶ ̶t̶o̶ ̶a̶c̶c̶e̶p̶t̶ ̶a̶n̶ ̶o̶p̶t̶i̶o̶n̶a̶l̶ ̶a̶r̶r̶a̶y̶.̶ ̶T̶h̶a̶t̶ ̶m̶a̶y̶ ̶a̶c̶t̶u̶a̶l̶l̶y̶ ̶h̶a̶v̶e̶ ̶s̶i̶d̶e̶ ̶e̶f̶f̶e̶c̶t̶s̶ ̶t̶h̶a̶t̶ ̶a̶r̶e̶ ̶u̶n̶a̶c̶c̶o̶u̶n̶t̶e̶d̶ ̶f̶o̶r̶,̶ ̶w̶h̶i̶c̶h̶ ̶i̶s̶ ̶c̶l̶e̶a̶r̶ ̶u̶p̶o̶n̶ ̶c̶o̶m̶p̶a̶r̶i̶s̶o̶n̶ ̶o̶f̶ ̶E̶n̶t̶r̶y̶ ̶1̶ ̶a̶n̶d̶ ̶E̶n̶t̶r̶y̶ ̶3̶ ̶f̶o̶r̶ ̶a̶l̶l̶ ̶e̶n̶v̶i̶r̶o̶n̶m̶e̶n̶t̶s̶;̶ ̶a̶n̶d̶ ̶E̶n̶t̶r̶y̶ ̶2̶ ̶a̶n̶d̶ ̶E̶n̶t̶r̶y̶ ̶4̶ ̶f̶o̶r̶ ̶o̶n̶l̶y̶ ̶F̶i̶r̶e̶f̶o̶x̶.̶ ̶I̶f̶ ̶w̶e̶ ̶i̶g̶n̶o̶r̶e̶ ̶t̶h̶e̶ ̶o̶r̶d̶e̶r̶-̶o̶f̶-̶m̶a̶g̶n̶i̶t̶u̶d̶e̶ ̶d̶i̶f̶f̶e̶r̶e̶n̶c̶e̶ ̶a̶n̶d̶ ̶f̶o̶c̶u̶s̶ ̶o̶n̶ ̶t̶h̶e̶ ̶t̶r̶e̶n̶d̶s̶,̶ ̶i̶t̶ ̶a̶p̶p̶e̶a̶r̶s̶ ̶t̶h̶a̶t̶ ̶t̶h̶e̶ ̶o̶p̶t̶i̶m̶i̶s̶a̶t̶i̶o̶n̶ ̶u̶s̶e̶d̶ ̶i̶n̶ ̶d̶i̶f̶f̶e̶r̶e̶n̶t̶ ̶e̶n̶g̶i̶n̶e̶s̶ ̶s̶e̶e̶n̶ ̶h̶e̶r̶e̶ ̶c̶o̶r̶r̶o̶b̶o̶r̶a̶t̶e̶s̶ ̶w̶h̶a̶t̶ ̶@̶l̶o̶n̶d̶o̶n̶s̶_̶e̶x̶p̶l̶o̶r̶e̶ ̶h̶a̶s̶ ̶a̶l̶r̶e̶a̶d̶y̶ ̶p̶o̶i̶n̶t̶e̶d̶ ̶o̶u̶t̶.̶\u003cp\u003eThere is also a difference between the times for array and objects across different environment, with an object generally taking longer. This result isn't directly related to our primary interest here, but I thought it was interesting and worth pointing out because of object-related concept in JavaScript.\u003cp\u003eFinally, if we use a pre-sorted array of indices to access elements in the original array containing instances of `Boom`, we see a substantial reduction in the time for Firefox but not \u003cem\u003eNodeJS\u003c/em\u003e and Chrome. This result was a bit \u0026quot;unexpected\u0026quot; but, again, this comes back to the point @londons_explore made.\u003cp\u003eEdit: updated code to allow the environment to be changed.\nEdit 2: Edited according to the of @minitech's comment.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Memory in JavaScript – Beyond Leaks (2019)",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://medium.com/walkme-engineering/memory-in-javascript-beyond-leaks-8c1d697c655c",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T08:50:14.000Z",
      title: null,
      url: null,
      author: "ru6xul6",
      points: null,
      story_text: null,
      comment_text:
        'I made a code base visualizer for Javascript, Typescript, and Python. Functions are shown as nodes, while function calls are lines connecting nodes.\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;codemap.app" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;codemap.app\u003c/a\u003e\u003cp\u003eAny feedback is welcome :)',
      num_comments: null,
      story_id: 23592788,
      story_title: "Ask HN: What did you make during lockdown?",
      story_url: null,
      parent_id: 23592788,
      created_at_i: 1592815814,
      _tags: ["comment", "author_ru6xul6", "story_23592788"],
      objectID: "23598367",
      _highlightResult: {
        author: {
          value: "ru6xul6",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            'I made a code base visualizer for Javascript, Typescript, and Python. Functions are shown as \u003cem\u003enodes\u003c/em\u003e, while function calls are lines connecting \u003cem\u003enodes\u003c/em\u003e.\u003cp\u003e\u003ca href="https://codemap.app" rel="nofollow"\u003ehttps://codemap.app\u003c/a\u003e\u003cp\u003eAny feedback is welcome :)',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Ask HN: What did you make during lockdown?",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T07:31:24.000Z",
      title: null,
      url: null,
      author: "maxbaines",
      points: null,
      story_text: null,
      comment_text:
        'I second this, of course VS Code needed Electron which needs Node all ready for ARM \u003ca href="https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;nodejs\u0026#x2F;node\u0026#x2F;issues\u0026#x2F;25998#issuecomment-637111448" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;nodejs\u0026#x2F;node\u0026#x2F;issues\u0026#x2F;25998#issuecomment-637...\u003c/a\u003e \nAlso to note Edge is available on ARM. Very happy Surface Pro X user here.',
      num_comments: null,
      story_id: 23597403,
      story_title: "ARM Mac Impact on Intel",
      story_url: "https://mondaynote.com/arm-mac-impact-on-intel-9641a8e73dca",
      parent_id: 23597845,
      created_at_i: 1592811084,
      _tags: ["comment", "author_maxbaines", "story_23597403"],
      objectID: "23598004",
      _highlightResult: {
        author: {
          value: "maxbaines",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            'I second this, of course VS Code needed Electron which needs Node all ready for ARM \u003ca href="https://github.com/nodejs/node/issues/25998#issuecomment-637111448" rel="nofollow"\u003ehttps://github.com/\u003cem\u003enodejs\u003c/em\u003e/node/issues/25998#issuecomment-637...\u003c/a\u003e \nAlso to note Edge is available on ARM. Very happy Surface Pro X user here.',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "ARM Mac Impact on Intel",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://mondaynote.com/arm-mac-impact-on-intel-9641a8e73dca",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T06:35:33.000Z",
      title: null,
      url: null,
      author: "ryan-allen",
      points: null,
      story_text: null,
      comment_text:
        "That is really interesting. Most of the developers I know use Mac OS for nodejs tooled web development, and they use Visual Studio Code. VS Code hasn\u0026#x27;t got an official ARM build yet.",
      num_comments: null,
      story_id: 23597403,
      story_title: "ARM Mac Impact on Intel",
      story_url: "https://mondaynote.com/arm-mac-impact-on-intel-9641a8e73dca",
      parent_id: 23597765,
      created_at_i: 1592807733,
      _tags: ["comment", "author_ryan-allen", "story_23597403"],
      objectID: "23597785",
      _highlightResult: {
        author: {
          value: "ryan-allen",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "That is really interesting. Most of the developers I know use Mac OS for \u003cem\u003enodejs\u003c/em\u003e tooled web development, and they use Visual Studio Code. VS Code hasn't got an official ARM build yet.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "ARM Mac Impact on Intel",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://mondaynote.com/arm-mac-impact-on-intel-9641a8e73dca",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T02:05:31.000Z",
      title: null,
      url: null,
      author: "zodiakzz",
      points: null,
      story_text: null,
      comment_text:
        "Deno\u0026#x27;s main selling point over Node.js seems to be some dubious sandboxing. Unlikely to be compelling enough for people to rewrite their applications from using Node\u0026#x27;s stdlib to Deno\u0026#x27;s.\u003cp\u003ePersonally, it makes me cringe to see hard-coded HTTP URLs in source code files for loading dependencies. They present it like we\u0026#x27;re stupid for using package managers all this time. What used to be a simple command line option (changing a package repo) is now a thousand file edit. Excellent.",
      num_comments: null,
      story_id: 23592483,
      story_title: "Deno will stop using TypeScript",
      story_url: "https://startfunction.com/deno-will-stop-using-typescript/",
      parent_id: 23593317,
      created_at_i: 1592791531,
      _tags: ["comment", "author_zodiakzz", "story_23592483"],
      objectID: "23596855",
      _highlightResult: {
        author: {
          value: "zodiakzz",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "Deno's main selling point over Node.js seems to be some dubious sandboxing. Unlikely to be compelling enough for people to rewrite their applications from using \u003cem\u003eNode's\u003c/em\u003e stdlib to Deno's.\u003cp\u003ePersonally, it makes me cringe to see hard-coded HTTP URLs in source code files for loading dependencies. They present it like we're stupid for using package managers all this time. What used to be a simple command line option (changing a package repo) is now a thousand file edit. Excellent.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Deno will stop using TypeScript",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://startfunction.com/deno-will-stop-using-typescript/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-22T00:21:27.000Z",
      title: null,
      url: null,
      author: "anonytrary",
      points: null,
      story_text: null,
      comment_text:
        "Square matrices are also a natural description for graphs (e.g. adjacency matrices), which are fundamental objects for pretty much \u003ci\u003eall\u003c/i\u003e CS applications across all layers of your tech stack. Square matrices are great for representing pair-wise interactions between nodes.",
      num_comments: null,
      story_id: 23591553,
      story_title: "Why is it important for a matrix to be square? (2018)",
      story_url: "https://math.stackexchange.com/a/2811960/18947",
      parent_id: 23596114,
      created_at_i: 1592785287,
      _tags: ["comment", "author_anonytrary", "story_23591553"],
      objectID: "23596409",
      _highlightResult: {
        author: {
          value: "anonytrary",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "Square matrices are also a natural description for graphs (e.g. adjacency matrices), which are fundamental objects for pretty much \u003ci\u003eall\u003c/i\u003e CS applications across all layers of your tech stack. Square matrices are great for representing pair-wise interactions between \u003cem\u003enodes\u003c/em\u003e.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Why is it important for a matrix to be square? (2018)",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://math.stackexchange.com/a/2811960/18947",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-21T21:15:58.000Z",
      title: null,
      url: null,
      author: "kbenson",
      points: null,
      story_text: null,
      comment_text:
        "I\u0026#x27;m not entirely sure what you think I was saying. You refer to me assuming and being uncharitable in those assumptions, but I was responding to a specific list of items pointed out, and my own experience. I think you\u0026#x27;re actually assuming quite a bit about my comment.\u003cp\u003e\u0026gt; You don\u0026#x27;t think JS is obviously unique for its ubiquitous Promise, async\u0026#x2F;await, + async-everything abstraction. You tend to only get that in other languages (Rust, Python, Ruby) by limiting yourself to a fraction of the ecosystem.\u003cp\u003eA fraction like nodejs or whatever subset of NPM you decide to use?\u003cp\u003eCore JS is what you get in a browser. That includes async\u0026#x2F;await, but it\u0026#x27;s hardly ubiquitous in usage in the core.\u003cp\u003eThe main distinction JS has over Perl, Python, Ruby etc is that it\u0026#x27;s in the browser, so you can assume almost everyone has it and it\u0026#x27;s accessible in some manner if they access a web property you are responsible for.",
      num_comments: null,
      story_id: 23592483,
      story_title: "Deno will stop using TypeScript",
      story_url: "https://startfunction.com/deno-will-stop-using-typescript/",
      parent_id: 23594731,
      created_at_i: 1592774158,
      _tags: ["comment", "author_kbenson", "story_23592483"],
      objectID: "23595475",
      _highlightResult: {
        author: {
          value: "kbenson",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "I'm not entirely sure what you think I was saying. You refer to me assuming and being uncharitable in those assumptions, but I was responding to a specific list of items pointed out, and my own experience. I think you're actually assuming quite a bit about my comment.\u003cp\u003e\u0026gt; You don't think JS is obviously unique for its ubiquitous Promise, async/await, + async-everything abstraction. You tend to only get that in other languages (Rust, Python, Ruby) by limiting yourself to a fraction of the ecosystem.\u003cp\u003eA fraction like \u003cem\u003enodejs\u003c/em\u003e or whatever subset of NPM you decide to use?\u003cp\u003eCore JS is what you get in a browser. That includes async/await, but it's hardly ubiquitous in usage in the core.\u003cp\u003eThe main distinction JS has over Perl, Python, Ruby etc is that it's in the browser, so you can assume almost everyone has it and it's accessible in some manner if they access a web property you are responsible for.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Deno will stop using TypeScript",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://startfunction.com/deno-will-stop-using-typescript/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-21T20:10:25.000Z",
      title: null,
      url: null,
      author: "NicoJuicy",
      points: null,
      story_text: null,
      comment_text:
        "No, I\u0026#x27;m experienced with node and other programming languages. I never said NodeJS is a bad language, that is not related to the statement i made.\u003cp\u003eI also didn\u0026#x27;t say everyone is lazy. I said a lot of developers.\u003cp\u003eMost people here do not belong to that use-case. As they are mostly here by interest and are willing to learn.\u003cp\u003eBut most people, after work, don\u0026#x27;t do anything with their knowledge.\u003cp\u003eAnd are more willing to learn node as it\u0026#x27;s the same on the backend + frontend.\u003cp\u003eOr keep using what they already know, with no interest for other languages.",
      num_comments: null,
      story_id: 23592483,
      story_title: "Deno will stop using TypeScript",
      story_url: "https://startfunction.com/deno-will-stop-using-typescript/",
      parent_id: 23594719,
      created_at_i: 1592770225,
      _tags: ["comment", "author_NicoJuicy", "story_23592483"],
      objectID: "23595000",
      _highlightResult: {
        author: {
          value: "NicoJuicy",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "No, I'm experienced with node and other programming languages. I never said \u003cem\u003eNodeJS\u003c/em\u003e is a bad language, that is not related to the statement i made.\u003cp\u003eI also didn't say everyone is lazy. I said a lot of developers.\u003cp\u003eMost people here do not belong to that use-case. As they are mostly here by interest and are willing to learn.\u003cp\u003eBut most people, after work, don't do anything with their knowledge.\u003cp\u003eAnd are more willing to learn node as it's the same on the backend + frontend.\u003cp\u003eOr keep using what they already know, with no interest for other languages.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Deno will stop using TypeScript",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://startfunction.com/deno-will-stop-using-typescript/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2020-06-21T19:37:56.000Z",
      title: null,
      url: null,
      author: "amaurymartiny",
      points: null,
      story_text: null,
      comment_text:
        "\u0026gt; mostly\u003cp\u003eUsing Tor, I can verify around ~60% of emails. Notably, the exit nodes can connect to Gmail\u0026#x2F;GSuite servers. So it\u0026#x27;s not that bad.\u003cp\u003eIf Tor\u0026#x27;s exit nodes are blocked by a server, I fallback to Heroku, where I have right now 3 fixed-IP instances. And if I see that one of these also gets blacklisted, I would have it self-destruct and relaunch, Heroku assigns a new IP on each new instance.",
      num_comments: null,
      story_id: 23592788,
      story_title: "Show HN: What did you make during lockdown?",
      story_url: null,
      parent_id: 23594624,
      created_at_i: 1592768276,
      _tags: ["comment", "author_amaurymartiny", "story_23592788"],
      objectID: "23594729",
      _highlightResult: {
        author: {
          value: "amaurymartiny",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "\u0026gt; mostly\u003cp\u003eUsing Tor, I can verify around ~60% of emails. Notably, the exit \u003cem\u003enodes\u003c/em\u003e can connect to Gmail/GSuite servers. So it's not that bad.\u003cp\u003eIf Tor's exit \u003cem\u003enodes\u003c/em\u003e are blocked by a server, I fallback to Heroku, where I have right now 3 fixed-IP instances. And if I see that one of these also gets blacklisted, I would have it self-destruct and relaunch, Heroku assigns a new IP on each new instance.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["nodejs"],
        },
        story_title: {
          value: "Show HN: What did you make during lockdown?",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
  ],
  nbHits: 14730,
  page: 0,
  nbPages: 50,
  hitsPerPage: 20,
  exhaustiveNbHits: false,
  query: "nodejs",
  params:
    "advancedSyntax=true\u0026analytics=true\u0026analyticsTags=backend\u0026query=nodejs",
  processingTimeMS: 7,
};

const dummyNews = news.hits
  .map(
    ({
      objectID,
      title,
      story_title,
      created_at_i,
      author,
      url,
      story_url,
    }) => {
      return {
        objectID: Number.parseInt(objectID),
        title: title ? title : story_title ? story_title : null,
        created_timestamp: created_at_i,
        author,
        url: story_url ? story_url : url ? url : null,
      };
    }
  )
  .filter((element) => element.objectID);

module.exports = dummyNews;
