import React from "react";
import NewsListComponent from "../components/NewsListComponent";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";

export const history = createBrowserHistory();
const AppRouter = () => (
  <Router history={history}>
    <Switch>
      <Route path="/home" component={NewsListComponent} />
      <Redirect from="*" to="/home" />
    </Switch>
  </Router>
);

export default AppRouter;
