import moment from "moment";

const parseDate = (unix) => {
  const time = moment.unix(unix);
  if (time.format("dd/MM/YYYY") === moment().format("dd/MM/YYYY")) {
    return time.format("hh:mm A");
  } else if (
    time.format("dd/MM/YYYY") === moment().subtract(1, "d").format("dd/MM/YYYY")
  ) {
    return "Yesterday";
  } else {
    return time.format("MMM D");
  }
};

export default parseDate;
