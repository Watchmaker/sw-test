import axios from "axios";

const removeNew = (id) => {
  return axios.put(`http://localhost:8080/news/${id}`);
};

export default removeNew;
