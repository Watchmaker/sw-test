import axios from "axios";

const fetchNews = () => {
  return axios
    .get(`http://localhost:8080/news`)
    .then((data) => {
      return data.data;
    })
    .catch((data) => {
      console.log(data.data);
      return [];
    });
};

export default fetchNews;
