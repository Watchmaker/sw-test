import React from "react";
import Header from "./Header";
import AppRouter from "../routers/AppRouter";
import Container from "@material-ui/core/Container";

const AppComponent = () => (
  <Container disableGutters={true} maxWidth="lg">
    <Header></Header>
    <AppRouter />
  </Container>
);

export default AppComponent;
