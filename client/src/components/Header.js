import React from "react";

const Header = () => (
  <header className="header">
    <p className="header__title">HN Feed</p>
    <p className="header__subtitle">{"We <3 Hacker News!"}</p>
  </header>
);

export default Header;
