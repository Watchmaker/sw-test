import React, { useEffect, useLayoutEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import NewComponent from "./NewComponent";
import fetchNews from "../utils/fetchNews";
import removeNew from "../utils/removeNew";

const NewsListComponent = () => {
  const [newsData, setNewsData] = useState([]);

  useEffect(() => {
    const run = async () => {
      const fecthedData = await fetchNews();
      setNewsData(fecthedData);
    };
    run();
  }, []);

  const deleteNew = async (id) => {
    await removeNew(id)
      .then(() => {
        console.log(id);
        const filtered = newsData.filter((element) => element.objectID != id);
        setNewsData(filtered);
      })
      .catch((e) => console.log(e));
  };
  return (
    <Box p={3} className="newsList">
      <Grid
        container
        direction="column"
        justify="space-evenly"
        alignItems="stretch"
        spacing={0}
      >
        {newsData.map((element, i) => {
          return (
            <div key={i} style={{ height: "60px" }}>
              <NewComponent data={element} removeNew={deleteNew} />
              <Divider />
            </div>
          );
        })}
      </Grid>
    </Box>
  );
};

export default NewsListComponent;
