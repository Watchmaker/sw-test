import React, { useState, useEffect } from "react";
import parseDate from "../utils/parseDate";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import DeleteIcon from "@material-ui/icons/Delete";

const NewComponent = (props) => {
  const [over, setOver] = useState(false);
  const [date, setDate] = useState(null);
  useEffect(() => {
    let created_at = parseDate(props.data.created_timestamp);
    setDate(created_at);
  }, []);
  return (
    <Box
      className="newComponent"
      p={1}
      width={"100%"}
      onClick={() => window.open(props.data.url, "_blank")}
      onMouseEnter={() => {
        setOver(true);
      }}
      onMouseLeave={() => {
        setOver(false);
      }}
    >
      <Grid container spacing={2}>
        <Grid item sm={9} className="newComponent__title">
          {props.data.title ? props.data.title : "Title"}
          <span> - {props.data.author} - </span>
        </Grid>
        <Grid item sm={2} className="newComponent__date">
          {date}
        </Grid>
        <Grid item sm={1}>
          <div hidden={!over}>
            <DeleteIcon
              className="newComponent__deleteIcon"
              onClick={(e) => {
                e.stopPropagation();
                props.removeNew(props.data.objectID);
              }}
            />
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};

export default NewComponent;
