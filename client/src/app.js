import React from "react";
import ReactDOM from "react-dom";
import AppComponent from "./components/AppComponent";
import "./styles/styles.scss";

const jsx = (
  <div>
    <AppComponent />
  </div>
);

ReactDOM.render(jsx, document.getElementById("app"));
